// File ctxChg.cpp
#include "ctxChg.h"
#include <stdlib.h>
#include <dos.h>
#include "lock.h"
#include "pcb.h"
#include "idleThr.h"
#include "SCHEDULE.H"
#include "thread.h"
#include <iostream.h>
#include "kernel.h"
#include "typedefs.h"

const int ContextChange::TIMER_IVT_ENTRY = 8;
volatile Word ContextChange::requested_ = 0;
volatile Time ContextChange::time_left_ = 0;
volatile int ContextChange::time_up_ = 0;

static volatile Register stack_segment;
static volatile Register stack_offset;
static volatile Register base_pointer;

void interrupt (*ContextChange::old_routine_)(...) = NULL;

void tick(); // User program must implement this function

void interrupt ContextChange::timerInterrupt(...) {
	if (!requested_) {
		callOldTimer();
		tick();
		if (!Kernel::instance_->semaphore_operation_) Kernel::instance_->semaphore_list_.updateTime();
		else Kernel::instance_->requested_semaphore_time_update_ = 1;
	}
	if (!requested_ && time_left_ == 1 && lock_ != 0) time_up_ = 1;
	
	if (requested_ || time_left_ != 0 && --time_left_ == 0 && lock_ == 0) {
		// Save current context
		if (PCB::running_ != NULL) {
			PCB::running_->my_lock_ = lock_; // Lock must be saved because the context change can be requested inside a locked block
			asm {
				mov stack_segment, ss;
				mov stack_offset, sp;
				mov base_pointer, bp;
				// The rest is on the stack
			}
			PCB::running_->stack_segment_ = stack_segment;
			PCB::running_->stack_offset_ = stack_offset;
			PCB::running_->base_pointer_ = base_pointer;
			
			
			// Put the current thread in the scheduler
			if (PCB::running_->state_ == PCB::RUNNING && PCB::running_->thread() != (Thread *) IdleThread::instance()) {
				PCB::running_->state_ = PCB::READY;
				Scheduler::put(PCB::running_);
			}
		}
		// Get the new thread from the scheduler
		PCB::running_ = Scheduler::get();
		if (PCB::running_ == NULL) PCB::running_ = PCB::getPCBByID(IdleThread::instance()->getId());
		PCB::running_->state_ = PCB::RUNNING;
		lock_ = PCB::running_->my_lock_; // Restore the lock
		time_left_ = PCB::running_->time_slice_;
		stack_segment = PCB::running_->stack_segment_;
		stack_offset = PCB::running_->stack_offset_;
		base_pointer = PCB::running_->base_pointer_;
		asm {
			mov ss, stack_segment;
			mov sp, stack_offset;
			mov bp, base_pointer;
		}
		requested_ = 0;
	}
}

void ContextChange::setNewInterrupt() {
	asm pushf;
	asm cli; // Mask all hardware interrupts
	old_routine_ = getvect(TIMER_IVT_ENTRY); // Save the old interrupt routine
	setvect(TIMER_IVT_ENTRY, timerInterrupt); // Set the new interrupt routine
	asm popf; // Unmask hardware interrupts
}

void ContextChange::restoreOldInterrupt() {
	asm pushf;
	asm cli; // Mask all hardware interrupts
	setvect(TIMER_IVT_ENTRY, old_routine_); // Restore old interrupt routine
	asm popf; // Unmask hardware interrupts
}
