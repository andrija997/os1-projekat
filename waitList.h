// File: waitList.h
#ifndef _waitlist_h_
#define _waitlist_h_

#include "typedefs.h" // Time
#include <stdlib.h> // NULL

class PCB;

class WaitList {
public:
	WaitList(): first_(NULL) {}
	volatile WaitList &insert(PCB *pcb, Time time) volatile;
	int updateTime() volatile; // Returns the number of woken up threads
	volatile WaitList &putBackInScheduler() volatile;
	int isEmpty() const { return first_ == NULL; }
	PCB *get() volatile; // Removes one element and returns it
	~WaitList() { putBackInScheduler(); }
private:
	struct WaitListElement {
		PCB *pcb_;
		Time time_;
		WaitListElement *next_;
		WaitListElement(PCB *pcb, Time time, WaitListElement *next): pcb_(pcb), time_(time), next_(next) {}
	};
	WaitListElement *first_;
	

};

#endif
