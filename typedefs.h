// File: typedefs.h
#ifndef _typedefs_h_
#define _typedefs_h_

typedef unsigned long StackSize;
typedef unsigned int Time;
typedef int ID;
typedef unsigned int Word;
typedef unsigned int Register;
typedef unsigned char IVTNo;

#endif
