// File: pcb.h
#ifndef _pcb_h_
#define _pcb_h_

#include "typedefs.h" // ID, Word, Register
#include "pcbList.h" // PCBList

class Thread;

class PCB {
public:
	friend class ContextChange; // Must be able to completely alter the PCB
	friend int main(int argc, char *argv[]); // Set the main PCB
	friend class KernelSem; // Set and reset the woken_up_by_signal_ flag
	friend class Thread; // Needs access because of forking
	friend class Fork; // Needs access to the stack
	
	enum State {
		NEW,
		READY,
		RUNNING,
		BLOCKED,
		COMPLETE
	};

	PCB(Thread *thread, StackSize stack_size, Time time_slice);
	
	Thread *thread() const { return thread_; }
	ID id() const { return thread_ID_; }
	
	static PCB *getPCBByID(ID id);
	
	static PCB *running() { return running_; }
	
	void setState(State state) { state_ = state; }
	
	void waitToComplete();
	
	void interrupt copyStack();
	
	~PCB();
	
private:
	// Basic thread info:
	Thread *thread_;
	StackSize stack_size_;
	Time time_slice_;
	
	// Context:
	Word *stack_;
	volatile Register stack_segment_;
	volatile Register stack_offset_;
	volatile Register base_pointer_;
	volatile Word my_lock_;
	
	volatile State state_;
	
	static ID thread_ID_counter_;
	ID thread_ID_;
	
	PCBList waiting_list_;
	
	volatile int woken_up_by_signal_;
	
	PCB *parent_;
	unsigned number_of_children_;
	
	static PCB *running_;
	
	static const StackSize MIN_STACK_SIZE;
	static const StackSize MAX_STACK_SIZE;
	
	void initializeStack(StackSize stack_size);
	
	// Method used to run the thread
	static void wrapper();
	
};

#endif
