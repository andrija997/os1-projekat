// File krnlSem.cpp
#include "krnlSem.h"
#include "lock.h"
#include "pcb.h"
#include <stdlib.h>
#include "SCHEDULE.H"
#include <iostream.h>
#include "kernel.h"
#include "typedefs.h"
#include "thread.h"

KernelSem::KernelSem(int init): value_(init) {
	lock;
	Kernel::instance()->semaphore_operation_ = 1;
	
	Kernel::instance()->semaphore_list_.insert(this);
	
	Kernel::instance()->semaphore_operation_ = 0;
	if (Kernel::instance()->requested_semaphore_time_update_) {
		Kernel::instance()->semaphore_list_.updateTime();
		Kernel::instance()->requested_semaphore_time_update_ = 0;
	}
	unlock;
}

int KernelSem::wait(Time max_time_to_wait) {
	lock;
	int ret;
	if (--value_ < 0) ret = block(max_time_to_wait);
	else ret = 1;
	unlock;
	return ret;
}

void KernelSem::signal() {
	lock;
	if (value_++ < 0) deblock();
	unlock;
}

KernelSem::~KernelSem() {
	lock;
	Kernel::instance()->semaphore_operation_ = 1;
	unlimited_waiting_list_.putBackInScheduler();
	limited_waiting_list_.putBackInScheduler();
	
	//asm cli;
	Kernel::instance()->semaphore_list_.remove(this);
	//asm sti;
	
	Kernel::instance()->semaphore_operation_ = 0;
	if (Kernel::instance()->requested_semaphore_time_update_) {
		Kernel::instance()->semaphore_list_.updateTime();
		Kernel::instance()->requested_semaphore_time_update_ = 0;
	}
	unlock;
}

int KernelSem::block(Time max_time_to_wait) {
	lock;
	PCB::running_->setState(PCB::BLOCKED);
	if (max_time_to_wait == 0) unlimited_waiting_list_.insert(PCB::running_);
	else {
		Kernel::instance()->semaphore_operation_ = 1;
		
		//asm cli;
		limited_waiting_list_.insert(PCB::running_, max_time_to_wait);
		//asm sti;
		
		Kernel::instance()->semaphore_operation_ = 0;
		if (Kernel::instance()->requested_semaphore_time_update_) {
			Kernel::instance()->semaphore_list_.updateTime();
			Kernel::instance()->requested_semaphore_time_update_ = 0;
		}
	}
	dispatch();
	int ret = PCB::running_->woken_up_by_signal_;
	PCB::running_->woken_up_by_signal_ = 0;
	unlock;
	return ret;
}

void KernelSem::deblock() {
	lock;
	PCB *woken_up_pcb;
	if (!unlimited_waiting_list_.isEmpty()) woken_up_pcb = unlimited_waiting_list_.get();
	else {
		Kernel::instance()->semaphore_operation_ = 1;
		
		//asm cli;
		woken_up_pcb = limited_waiting_list_.get();
		//asm sti;
		
		Kernel::instance()->semaphore_operation_ = 0;
		if (Kernel::instance()->requested_semaphore_time_update_) {
			Kernel::instance()->semaphore_list_.updateTime();
			Kernel::instance()->requested_semaphore_time_update_ = 0;
		}
	}
	if (woken_up_pcb != NULL) {
		woken_up_pcb->setState(PCB::READY);
		woken_up_pcb->woken_up_by_signal_ = 1;
		Scheduler::put(woken_up_pcb);
	}
	else {
		cout << "Nothing here" << endl;
	}
	unlock;
}
