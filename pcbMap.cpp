// File pcbMap.cpp
#include "pcbMap.h"
#include "thread.h"
#include <stdlib.h>
#include "pcb.h"
#include "thread.h"
#include "typedefs.h"

const int PCBMap::STARTING_VECTOR_SIZE = 16;

PCBMap::PCBMap(): number_of_elements_(0), vector_size_(STARTING_VECTOR_SIZE) {
	vector_ = new PCB*[STARTING_VECTOR_SIZE];
	for (int i = 0; i < STARTING_VECTOR_SIZE; i++) vector_[i] = NULL;
}

PCB *PCBMap::at(ID id) const {
	if (id > vector_size_) return NULL;
	return vector_[id];
}

PCBMap &PCBMap::remove(ID id) {
	if (id < vector_size_) vector_[id] = NULL;
	number_of_elements_--;
	return *this;
}

PCBMap &PCBMap::insert(ID id, PCB *pcb) {
	int i; // Compiler reporting for multiple declarations if defined in both for loops
	if (id > vector_size_) {
		// Reallocate space
		int new_vector_size = 2 * vector_size_;
		if (id > new_vector_size) new_vector_size = id + 1;
		PCB **new_vector = new PCB*[new_vector_size];
		for (i = 0; i < vector_size_; i++) new_vector[i] = vector_[i];
		for (i = vector_size_; i < new_vector_size; i++) new_vector[i] = NULL;
		delete [] vector_;
		vector_ = new_vector;
		vector_size_ = new_vector_size;
	}
	vector_[id] = pcb;
	number_of_elements_++;
	return *this;
}
