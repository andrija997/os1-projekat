// File ivtEntry.h
#ifndef _ivtentry_h_
#define _ivtentry_h_

#include "typedefs.h" // IVTNo

class KernelEv;

class IVTEntry {
public:
	IVTEntry(IVTNo id, void interrupt (*new_routine)(...));
	void setEvent(KernelEv *my_event) { my_event_ = my_event; }
	void callOldRoutine();
	void restoreOldRoutine();
	void signal();
	~IVTEntry();
private:
	IVTNo id_;
	void interrupt (*old_interrupt_routine_)(...);
	KernelEv *my_event_;
};

#endif
