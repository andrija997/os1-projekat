// File: kernel.h
#ifndef _kernel_h_
#define _kernel_h_

#include <stdlib.h> // NULL
#include "pcbMap.h" // PCBMap
#include "semList.h" // SemaphoreList
#include "ivTable.h" // IVTable

class Kernel {
public:
	static Kernel *instance() {
		if (instance_ == NULL) instance_ = new Kernel();
		return instance_;
	}
	
	int threadCount() const { return pcb_map_.numberOfElements(); }
	
private:
	friend class PCB; // Must be able to insert itself into the map
	friend class KernelSem; // Must be able to insert itself into the list
	friend class ContextChange; // Must be able to set and get the semaphore flags
	friend class IVTEntry; // Must be able to insert itself into the table
	friend class KernelEv; // Must be able to set its interrupt routine
	static Kernel *instance_;
	
	Kernel();
	Kernel(const Kernel &) {}
	void operator=(const Kernel &) {}
	
	volatile int semaphore_operation_;
	volatile int requested_semaphore_time_update_;
	
	PCBMap pcb_map_;
	volatile SemaphoreList semaphore_list_;
	
	IVTable interrupt_vector_table_;
	
};

#endif
