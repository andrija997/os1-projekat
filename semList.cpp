// File semList.cpp
#include "semList.h"
#include "lock.h"
#include "krnlsem.h"
#include <stdlib.h>
#include <iostream.h>
#include "pcb.h"
#include "thread.h"

volatile SemaphoreList &SemaphoreList::insert(KernelSem *sem) volatile {
	lock;
	SemaphoreListElement *new_element = new SemaphoreListElement(sem, first_, NULL);
	if (first_ != NULL) first_->prev_ = new_element;
	first_ = new_element;
	unlock;
	return *this;
}

volatile SemaphoreList &SemaphoreList::remove(KernelSem *sem) volatile {
	lock;
	SemaphoreListElement *temp = find(sem);
	if (temp->next_ != NULL) temp->next_->prev_ = temp->prev_;
	if (temp->prev_ != NULL) temp->prev_->next_ = temp->next_;
	delete temp;
	unlock;
	return *this;
}

volatile SemaphoreList &SemaphoreList::updateTime() volatile {
	lock;
	SemaphoreListElement *curr = first_;
	while (curr != NULL) {
		curr->sem_->updateTime();
		curr = curr->next_;
	}
	unlock;
	return *this;
}

SemaphoreList::SemaphoreListElement *SemaphoreList::find(KernelSem *sem) volatile {
	lock;
	SemaphoreListElement *curr = first_;
	while (curr != NULL && curr->sem_ != sem) curr = curr->next_;
	unlock;
	return curr;
}
