// File: semaphor.cpp
#include "semaphor.h"
#include "krnlSem.h"
#include "lock.h"
#include "typedefs.h"
#include "thread.h"

Semaphore::Semaphore(int init) {
	lock;
	myImpl = new KernelSem(init);
	unlock;
}

Semaphore::~Semaphore () {
	lock;
	delete myImpl;
	unlock;
}

int Semaphore::wait(Time maxTimeToWait) {
	lock;
	int ret = myImpl->wait(maxTimeToWait);
	unlock;
	return ret;
}

void Semaphore::signal() {
	lock;
	myImpl->signal();
	unlock;
}

int Semaphore::val() const {
	lock;
	int ret = myImpl->value();
	unlock;
	return ret;
}
