// File krnlEv.h
#ifndef _krnlev_h_
#define _krnlev_h_

#include "typedefs.h" // IVTNo

class PCB;

class KernelEv {
public:
	KernelEv(IVTNo id);
	void wait();
	void signal();
	~KernelEv();
private:
	IVTNo id_;
	PCB *my_PCB_;
	volatile int value_; // In range { -1, 0, 1 }
	
	void block();
	void deblock();
};

#endif
