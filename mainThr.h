// File: mainThr.h
#ifndef _mainthr_h_
#define _mainthr_h_

#include "thread.h" // Thread
#include <stdlib.h> // NULL

class MainThread : public Thread {
public:
	static MainThread *instance() {
		if (instance_ == NULL) instance_ = new MainThread();
		return instance_;
	}
	static void setArgs(int argc, char *argv[]);
	static int returnValue() { return return_value_; }
	virtual void run();
	virtual Thread *clone() const;
	~MainThread() { waitToComplete(); }
private:
	static MainThread *instance_;
	static int argc_;
	static char **argv_;
	static int return_value_;
	MainThread() {} // Default value for the stack size and the time slice
	MainThread(const MainThread &) {}
	void operator=(const MainThread &) {}
};

#endif
