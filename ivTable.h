// File: ivTable.h
#ifndef _ivtable_h_
#define _ivtable_h_

#include "typedefs.h" // IVTNo

class IVTEntry;

class IVTable {
public:
	IVTable();
	IVTEntry *&operator[](IVTNo index) { return array_[index]; }
private:
	// Static const members of integral type with initializers in class were not supported until C++98
	// This behaves exactly the same as:
	// static const int NUM_OF_INTERRUPT_ENTRIES = 256;
	enum { NUM_OF_INTERRUPT_ENTRIES = 256 };
	
	IVTEntry *array_[NUM_OF_INTERRUPT_ENTRIES];
};

#endif
