// File: pcbMap.h
#ifndef _pcbmap_h_
#define _pcbmap_h_

#include "typedefs.h" // ID

class PCB;

class PCBMap {
public:
	PCBMap();
	PCB *at(ID id) const;
	PCBMap &remove(ID id);
	PCBMap &insert(ID id, PCB *pcb);
	int numberOfElements() const { return number_of_elements_; }
private:
	static const int STARTING_VECTOR_SIZE;
	PCB **vector_;
	int vector_size_;
	int number_of_elements_;
};

#endif
