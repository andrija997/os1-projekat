// File: pcbList.h
#ifndef _pcblist_h_
#define _pcblist_h_

#include <stdlib.h> //NULL

class PCB;

class PCBList {
public:
	PCBList(): first_(NULL), last_(NULL) {}
	PCBList &insert(PCB *pcb);
	PCBList &putBackInScheduler();
	PCB *get(); // Removes one element and returns it
	int isEmpty() const { return first_ == NULL; }
	~PCBList();
private:
	struct ListElement {
		PCB *pcb_;
		ListElement *next_;
		ListElement(PCB *pcb): pcb_(pcb), next_(NULL) {}
	};
	ListElement *first_;
	ListElement *last_;
};

#endif
