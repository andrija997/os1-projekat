// File: semList.h
#ifndef _semlist_h_
#define _semlist_h_

#include <stdlib.h> // NULL

class KernelSem;

class SemaphoreList {
public:
	SemaphoreList(): first_(NULL) {}
	volatile SemaphoreList &insert(KernelSem *sem) volatile;
	volatile SemaphoreList &remove(KernelSem *sem) volatile;
	volatile SemaphoreList &updateTime() volatile;
private:
	struct SemaphoreListElement {
		KernelSem *sem_;
		SemaphoreListElement *next_;
		SemaphoreListElement *prev_;
		SemaphoreListElement(KernelSem *sem, SemaphoreListElement *next, SemaphoreListElement *prev): sem_(sem), next_(next), prev_(prev) {}
	};
	
	SemaphoreListElement *first_;
	
	SemaphoreListElement *find(KernelSem *sem) volatile;
};

#endif