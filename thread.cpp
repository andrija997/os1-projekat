// File: thread.cpp
#include "thread.h"
#include <stdlib.h>
#include "pcb.h"
#include "lock.h"
#include "SCHEDULE.H"
#include "ctxChg.h"
#include <iostream.h>
#include "fork.h"
#include <stdio.h>

void Thread::start() {
	lock;
	myPCB->setState(PCB::READY);
	Scheduler::put(myPCB);
	unlock;
}

void Thread::waitToComplete() {
	lock;
	myPCB->waitToComplete();
	unlock;
}

Thread::~Thread() {
	lock;
	waitToComplete();
	delete myPCB;
	unlock;
}

ID Thread::getId() {
	return myPCB->id();
}

ID Thread::getRunningId() {
	return PCB::running()->id();
}

Thread *Thread::getThreadById(ID id) {
	return PCB::getPCBByID(id)->thread();
}

ID Thread::fork() {
	int ret;
	lock;
	// Set the parent and child pointers:
	Fork::parent(PCB::running_);
	Thread *parent_thread = PCB::running_->thread_;
	Thread *child_thread = parent_thread->clone();
	Fork::child(child_thread->myPCB);
	parent_thread->myPCB->number_of_children_++;
	child_thread->myPCB->parent_ = parent_thread->myPCB;
	// Create the child context:
	Fork::copyStack();
	if (PCB::running_->thread_ID_ == parent_thread->myPCB->thread_ID_) {
		Scheduler:: put(child_thread->myPCB);
		ret = child_thread->myPCB->thread_ID_;
	}
	else if (PCB::running_->thread_ID_ == child_thread->myPCB->thread_ID_) ret = 0;
	else ret = -1;
	unlock;
	return ret;
}

void Thread::exit() {
	lock;
	PCB::running_->state_ = PCB::COMPLETE;
	PCB::running_->waiting_list_.putBackInScheduler();
	if (PCB::running_->parent_ != NULL) {
		PCB::running_->parent_->number_of_children_--;
		if (PCB::running_->parent_->number_of_children_ == 0) {
			PCB::running_->parent_->state_ = PCB::READY;
			Scheduler::put(PCB::running_->parent_);
		}
	}
	delete PCB::running_->thread_;
	PCB::running_->thread_ = NULL;
	dispatch(); // Will not come back from this dispatch - no need for the unlock
}

void Thread::waitForForkChildren() {
	if (PCB::running_->number_of_children_ > 0) {
		PCB::running_->state_ = PCB::BLOCKED;
		dispatch();
	}
}

Thread* Thread::clone() const {
	// Used in Thread::fork()
	// Not meant to be used by itself
	// All user threads should override this method, and this method should never be used
	return new Thread(myPCB->stack_size_ * 2, myPCB->time_slice_);
}

Thread::Thread (StackSize stackSize, Time timeSlice) {
	myPCB = new PCB(this, stackSize, timeSlice);
}

void dispatch() {
	asm pushf;
	asm cli;
	ContextChange::requestChange();
	ContextChange::timerInterrupt();
	asm popf;
}
