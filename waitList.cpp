// File: waitList.cpp
#include "waitList.h"
#include "lock.h"
#include <stdlib.h>
#include "SCHEDULE.H"
#include "pcb.h"
#include <iostream.h>
#include "thread.h"

volatile WaitList &WaitList::insert(PCB *pcb, Time time) volatile {
	lock;
	Time current_element_time = 0;
	WaitListElement *curr = first_;
	WaitListElement *prev = NULL;
	while (curr != NULL) {
		current_element_time += curr->time_;
		if (current_element_time >= time) {
			current_element_time -= curr->time_;
			break;
		}
		prev = curr;
		curr = curr->next_;
	}
	Time new_time;
	new_time = time - current_element_time;
	WaitListElement *new_element = new WaitListElement(pcb, new_time, curr);
	if (curr != NULL) curr->time_ -= new_time;
	if (prev != NULL) prev->next_ = new_element;
	else first_ = new_element;
	unlock;
	return *this;
}

int WaitList::updateTime() volatile {
	lock;
	int ret = 0;
	if (first_ != NULL) {
		first_->time_--;
		while (first_ != NULL && first_->time_ == 0) {
			WaitListElement *temp = first_;
			first_ = first_->next_;
			PCB *woken_up_pcb = temp->pcb_;
			woken_up_pcb->setState(PCB::READY);
			Scheduler::put(woken_up_pcb);
			delete temp;
			temp = first_;
			ret++;
		}
	}
	unlock;
	return ret;
}

volatile WaitList &WaitList::putBackInScheduler() volatile {
	lock;
	WaitListElement *curr = first_;
	while (curr != NULL) {
		first_ = first_->next_;
		curr->pcb_->setState(PCB::READY);
		Scheduler::put(curr->pcb_);
		delete curr;
		curr = first_;
	}
	unlock;
	return *this;
}

PCB *WaitList::get() volatile {
	lock;
	WaitListElement *old_first = first_;
	if (first_ != NULL) first_ = first_->next_;
	PCB *ret = NULL;
	if (old_first != NULL) {
		ret = old_first->pcb_;
		first_->time_ += old_first->time_;
		delete old_first;
	}
	unlock;
	return ret;
}
