// File: krnlEv.cpp
#include "krnlEv.h"
#include "pcb.h"
#include "SCHEDULE.H"
#include "lock.h"
#include "typedefs.h"
#include "thread.h"
#include <iostream.h>
#include "kernel.h"
#include "ivtEntry.h"

KernelEv::KernelEv(IVTNo id): id_(id), value_(0) {
	lock;
	my_PCB_ = PCB::running();
	Kernel::instance()->interrupt_vector_table_[id]->setEvent(this);
	unlock;
}

void KernelEv::wait() {
	lock;
	if (my_PCB_ == PCB::running() && value_-- == 0) block();
	unlock;
}

void KernelEv::signal() {
	lock;
	if (value_ < 1 && value_++ < 0) deblock();
	unlock;
}

KernelEv::~KernelEv() {
	Kernel::instance()->interrupt_vector_table_[id_]->restoreOldRoutine();
	Kernel::instance()->interrupt_vector_table_[id_] = NULL;
}

void KernelEv::block() {
	lock;
	my_PCB_->setState(PCB::BLOCKED);
	dispatch();
	unlock;
}

void KernelEv::deblock() {
	lock;
	my_PCB_->setState(PCB::READY);
	Scheduler::put(my_PCB_);
	unlock;
}
