// File fork.cpp
#include "fork.h"
#include "typedefs.h"
#include "pcb.h"
#include <iostream.h>
#include <dos.h>
#include <stdio.h>

PCB *Fork::parent_ = NULL;
PCB *Fork::child_ = NULL;

// Global variables used to not mess up the stack:

static int i;
static Register parent_stack_pointer;
static Register parent_base_pointer;

static unsigned parent_bp_iterator;
static unsigned child_bp_iterator;

static unsigned offset;

static Register parent_stack_segment;
static Register child_stack_segment;

static Word *parent_pointer;
static Word *child_pointer;

void interrupt Fork::copyStack(...) {
	// Raw copy of the parent's stack:
	for (i = 0; i < parent_->stack_size_; i++) child_->stack_[i] = parent_->stack_[i];
	// Set the base pointer iterators:
	parent_stack_segment = FP_SEG(parent_->stack_);
	child_stack_segment = FP_SEG(child_->stack_);
	asm mov parent_bp_iterator, bp;
	offset = parent_bp_iterator - FP_OFF(parent_->stack_);
	child_bp_iterator = FP_OFF(child_->stack_ + offset);
	// Iterate through the stack:
	while (parent_bp_iterator != 0) {
		// Obtain the pointers:
		parent_pointer = (Word *)MK_FP(parent_stack_segment, parent_bp_iterator);
		offset = parent_bp_iterator - *parent_pointer;
		child_pointer = (Word *)MK_FP(child_stack_segment, child_bp_iterator);
		// Fix the value:
		*child_pointer = child_bp_iterator - offset;
		// Next iteration:
		parent_bp_iterator = *parent_pointer;
		child_bp_iterator = *child_pointer;
	}
	// Set the rest of the child's context:
	child_->stack_segment_ = FP_SEG(child_->stack_);
	asm mov parent_stack_pointer, sp;
	asm mov parent_base_pointer, bp;
	offset = parent_stack_pointer - FP_OFF(parent_->stack_);
	child_->stack_offset_ = FP_OFF(child_->stack_) + offset;
	offset = parent_base_pointer - FP_OFF(parent_->stack_);
	child_->base_pointer_ = FP_OFF(child_->stack_) + offset;
}
