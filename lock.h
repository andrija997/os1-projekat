// File: lock.h
#ifndef _lock_h_
#define _lock_h_

#include "typedefs.h" // Word
#include "ctxChg.h" // time_up_ flag
#include "pcb.h" // dispatch()

extern volatile Word lock_;

#define lock ++lock_;
#define unlock --lock_;\
if (lock_ == 0 && ContextChange::time_up_) { ContextChange::time_up_ = 0; dispatch(); }

#endif
