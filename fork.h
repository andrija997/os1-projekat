// File: fork.h
#ifndef _fork_h_
#define _fork_h_

class PCB;

// Utility class
class Fork {
public:
	static void interrupt copyStack(...);
	static void parent(PCB *parent_pcb) { parent_ = parent_pcb; }
	static void child(PCB *child_pcb) { child_ = child_pcb; }
	static PCB *parent() { return parent_; }
	static PCB *child() { return child_; }
private:
	Fork() {}
	Fork(Fork &) {}
	void operator=(Fork &) {}
	
	static PCB *parent_;
	static PCB *child_;
};

#endif
