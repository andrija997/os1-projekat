// File: pcb.cpp
#include "pcb.h"
#include "thread.h"
#include <dos.h>
#include "kernel.h"
#include "pcbMap.h"
#include "lock.h"
#include "pcbList.h"
#include "idleThr.h"
#include <iostream.h>
#include "SCHEDULE.H"
#include <stdio.h>

ID PCB::thread_ID_counter_ = 0;
const StackSize PCB::MIN_STACK_SIZE = 0x400; // 1 KiB
const StackSize PCB::MAX_STACK_SIZE = 0x1000; // 64 KiB
PCB *PCB::running_ = NULL;

PCB::PCB(Thread *thread, StackSize stack_size, Time time_slice): 
																thread_(thread), 
																stack_size_(stack_size), 
																time_slice_(time_slice), 
																state_(NEW), 
																my_lock_(0), 
																thread_ID_(thread_ID_counter_++),
																woken_up_by_signal_(0),
																parent_(NULL),
																number_of_children_(0)
{
	initializeStack(stack_size);
	Kernel::instance()->pcb_map_.insert(thread_ID_, this);
	
}

PCB *PCB::getPCBByID(ID id) {
	return Kernel::instance()->pcb_map_.at(id);
}

void PCB::waitToComplete() {
	lock;
	if (running_ == this || state_ == COMPLETE || thread_ == IdleThread::instance()) {
		// Thread can't wait on itself, doesn't need to wait on a completed thread, and shouldn't wait on the idle thread
		unlock;
		return;
	}
	running_->state_ = BLOCKED;
	waiting_list_.insert(running_);
	dispatch();
	unlock;
}

void PCB::initializeStack(StackSize stack_size) {	
	// Correction if stack size exceeds limits:
	if (stack_size < MIN_STACK_SIZE) stack_size = MIN_STACK_SIZE;
	if (stack_size > MAX_STACK_SIZE) stack_size = MAX_STACK_SIZE;

	stack_size = stack_size / sizeof(Word); // stack_size_ initialized in bytes
	stack_size_ = stack_size;
	
	stack_ = new Word[stack_size];
	
	// Stack: psw, cs, ip, ax, bx, cx, dx, es, ds, si, di, bp
	
	// Initialize stack for first use:
	
	stack_[stack_size - 1] = 0x200; // PSWI == 1
	
	// When the thread is taken from the scheduler for the first time,
	// it should run the wrapper method:
	stack_[stack_size - 2] = FP_SEG(PCB::wrapper); // Code segment
	stack_[stack_size - 3] = FP_OFF(PCB::wrapper); // Instruction pointer
	
	stack_[stack_size - 12] = 0; // Base pointer - 0 because of fork
	
	// The relevant registers:
	stack_segment_ = FP_SEG(stack_ + stack_size - 12);
	stack_offset_ = FP_OFF(stack_ + stack_size - 12);
	base_pointer_ = FP_OFF(stack_ + stack_size - 12);
	
	// Other registers irrelevant
}

PCB::~PCB() {
	delete[] stack_;
	Kernel::instance()->pcb_map_.remove(thread_ID_);
}

void PCB::wrapper() {
	running_->thread_->run();
	lock;
	running_->state_ = COMPLETE;
	running_->waiting_list_.putBackInScheduler();
	if (running_->parent_ != NULL) {
		running_->parent_->number_of_children_--;
		if (running_->parent_->number_of_children_ == 0) {
			running_->parent_->state_ = PCB::READY;
			Scheduler::put(running_->parent_);
		}
	}
	dispatch(); // Will not come back from this dispatch - no need for the unlock
}
