// File: ivtEntry.cpp
#include "ivtEntry.h"
#include <dos.h>
#include <stdlib.h>
#include "kernel.h"
#include "lock.h"
#include "typedefs.h"
#include "thread.h"
#include "krnlEv.h"

IVTEntry::IVTEntry(IVTNo id, void interrupt (*new_routine)(...)): id_(id), my_event_(NULL) {
	lock;
	old_interrupt_routine_ = getvect(id);
	setvect(id, new_routine);
	Kernel::instance()->interrupt_vector_table_[id] = this;
	unlock;
}

void IVTEntry::callOldRoutine() {
	if (old_interrupt_routine_ != NULL) (*old_interrupt_routine_)();
}

void IVTEntry::signal() {
	lock;
	if (my_event_ != NULL) my_event_->signal();
	unlock;
}

void IVTEntry::restoreOldRoutine() {
	lock;
	if (old_interrupt_routine_ != NULL) {
		setvect(id_, old_interrupt_routine_);
		old_interrupt_routine_ = NULL;
	}
	unlock;
}

IVTEntry::~IVTEntry() {
	Kernel::instance()->interrupt_vector_table_[id_] = NULL;
	restoreOldRoutine();
}
