// File: idleThr.h
#ifndef _idlethr_h_
#define _idlethr_h_

#include "thread.h" // Thread
#include <stdlib.h> // NULL

class IdleThread : public Thread {	
public:
	static IdleThread *instance() {
		if (instance_ == NULL) instance_ = new IdleThread();
		return instance_;
	}
	virtual void run();
private:
	static IdleThread *instance_;
	IdleThread(): Thread(0, 1) {} // Stack size = 0, time slice = 1
	IdleThread(const IdleThread &) {}
	void operator=(const IdleThread &) {}
};

#endif
