// File ctxChg.h
#ifndef _ctxchg_h_
#define _ctxchg_h_

#include "typedefs.h" // Word, Time

// Utility class
class ContextChange {
public:
	static void interrupt timerInterrupt(...);
	static void setNewInterrupt();
	static void restoreOldInterrupt();
	volatile static void callOldTimer() { old_routine_(); }
	static void requestChange() { requested_ = 1; }
	
	volatile static int time_up_;
	
private:
	ContextChange() {}
	ContextChange(const ContextChange &) {}
	void operator=(const ContextChange &) {}
	
	static void interrupt (*old_routine_)(...); // Typedef is for pussies
	volatile static Word requested_;
	volatile static Time time_left_;
	
	static const int TIMER_IVT_ENTRY;
};

#endif
