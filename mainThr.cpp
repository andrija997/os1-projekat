// File: mainThr.cpp
#include "mainThr.h"
#include <stdlib.h>
#include "thread.h"

MainThread *MainThread::instance_ = NULL;
int MainThread::argc_ = 0;
char **MainThread::argv_ = NULL;
int MainThread::return_value_ = 0;

int userMain(int argc, char *argv[]);

void MainThread::setArgs(int argc, char **argv) {
	MainThread::argc_ = argc;
	MainThread::argv_ = argv;
}

void MainThread::run() {
	return_value_ = userMain(argc_, argv_);
}

Thread *MainThread::clone() const {
	return (Thread *)(new MainThread());
}