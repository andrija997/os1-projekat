// File: main.cpp
#include <iostream.h>
#include <stdlib.h>
#include "thread.h"
#include "lock.h"
#include "pcb.h"
#include "ctxChg.h"
#include "kernel.h"
#include "idleThr.h"
#include "semaphor.h"
#include "mainThr.h"

int main(int argc, char *argv[]) {
	
	cout << "Hello World!" << endl;
	
	// Prepare the PCB for the main function
	// It doesn't allocate a stack, and operates on the system stack
	PCB *main_pcb = new PCB(NULL, 0, 1);
	main_pcb->state_ = PCB::RUNNING;
	PCB::running_ = main_pcb;
	
	// Instantiate the Kernel threads, and the Kernel itself:
	Kernel::instance();
	IdleThread::instance();
	MainThread::instance();
	
	// Set the arguments for the user main thread:
	MainThread::instance()->setArgs(argc, argv);
	
	// Put the user main thread in the scheduler:
	MainThread::instance()->start();
	ContextChange::setNewInterrupt();
	
	// Wait for the user main thread to complete, and then delete it:
	delete MainThread::instance();

	ContextChange::restoreOldInterrupt();
	
	int ret = MainThread::returnValue();
	
	cout << endl << "Good Bye World!" << endl;
	
	// Delete the Kernel threads, and the Kernel itself:
	delete main_pcb;
	delete IdleThread::instance();
	delete Kernel::instance();
	
	return ret;
}
