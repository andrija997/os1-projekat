// File: krnlSem.h
#ifndef _krnlsem_h_
#define _krnlsem_h_

#include "typedefs.h" // Time
#include "pcbList.h" // PCBList
#include "waitList.h" // WaitList

class KernelSem {
public:
	KernelSem(int init);
	int wait(Time max_time_to_wait);
	void signal();
	int value() const { return value_; }
	void updateTime() volatile { value_ += limited_waiting_list_.updateTime(); }
	~KernelSem();
private:
	volatile int value_;
	PCBList unlimited_waiting_list_;
	volatile WaitList limited_waiting_list_;
	
	int block(Time max_time_to_wait);
	void deblock();
};

#endif
