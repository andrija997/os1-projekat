// File: ivTable.cpp
#include "ivTable.h"
#include <stdlib.h>
#include "ivtEntry.h"

IVTable::IVTable() {
	for (int i = 0; i < NUM_OF_INTERRUPT_ENTRIES; i++) array_[i] = NULL;
}
