// File: pcbList.cpp
#include "pcbList.h"
#include <stdlib.h>
#include "pcb.h"
#include "SCHEDULE.H"
#include "lock.h"
#include "thread.h"

PCBList &PCBList::insert(PCB *pcb) {
	lock;
	ListElement *new_element = new ListElement(pcb);
	if (first_ == NULL) first_ = new_element;
	else last_->next_ = new_element;
	last_ = new_element;
	unlock;
	return *this;
}
PCBList &PCBList::putBackInScheduler() {
	lock;
	ListElement *curr = first_;
	while (curr != NULL) {
		first_ = first_->next_;
		curr->pcb_->setState(PCB::READY);
		Scheduler::put(curr->pcb_);
		delete curr;
		curr = first_;
	}
	last_ = NULL;
	unlock;
	return *this;
}

PCB *PCBList::get() {
	lock;
	ListElement *old_first = first_;
	if (first_ != NULL) first_ = first_->next_;
	PCB *ret = NULL;
	if (old_first != NULL) {
		ret = old_first->pcb_;
		delete old_first;
	}
	if (first_ == NULL) last_ = NULL;
	unlock;
	return ret;
}

PCBList::~PCBList() {
	putBackInScheduler();
}
