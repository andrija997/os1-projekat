// File: idleThr.cpp
#include "idleThr.h"
#include "kernel.h"
#include "thread.h"
#include <stdlib.h>
#include <iostream.h>
#include "lock.h"

IdleThread *IdleThread::instance_ = NULL;

void IdleThread::run() {
	// 2 == Main Thread (just the PCB actually) + Idle Thread
	while (Kernel::instance()->threadCount() > 2);
}
